.. Pireal Project documentation master file, created by
   sphinx-quickstart on Sun Jul 12 15:36:49 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pireal Project's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 1
   
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

