src.gui package
===============

Subpackages
-----------

.. toctree::

    src.gui.query_editor

Submodules
----------

src.gui.actions module
----------------------

.. automodule:: src.gui.actions
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.container module
------------------------

.. automodule:: src.gui.container
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.lateral_widget module
-----------------------------

.. automodule:: src.gui.lateral_widget
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.main_window module
--------------------------

.. automodule:: src.gui.main_window
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.menu_actions module
---------------------------

.. automodule:: src.gui.menu_actions
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.new_relation_dialog module
----------------------------------

.. automodule:: src.gui.new_relation_dialog
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.start_page module
-------------------------

.. automodule:: src.gui.start_page
    :members:
    :undoc-members:
    :show-inheritance:

src.gui.table_widget module
---------------------------

.. automodule:: src.gui.table_widget
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.gui
    :members:
    :undoc-members:
    :show-inheritance:
