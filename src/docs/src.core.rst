src.core package
================

Submodules
----------

src.core.parser module
----------------------

.. automodule:: src.core.parser
    :members:
    :undoc-members:
    :show-inheritance:

src.core.relation module
------------------------

.. automodule:: src.core.relation
    :members:
    :undoc-members:
    :show-inheritance:

src.core.settings module
------------------------

.. automodule:: src.core.settings
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.core
    :members:
    :undoc-members:
    :show-inheritance:
