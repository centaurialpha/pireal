API Documentation
=================

Contents:

.. toctree::
   :maxdepth: 2
   
   src
   src.core
   src.gui
         
