src package
===========

Subpackages
-----------

.. toctree::

    src.core
    src.gui

Submodules
----------

src.keymap module
-----------------

.. automodule:: src.keymap
    :members:
    :undoc-members:
    :show-inheritance:

src.resources module
--------------------

.. automodule:: src.resources
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src
    :members:
    :undoc-members:
    :show-inheritance:
