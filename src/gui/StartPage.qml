import QtQuick 1.1

Rectangle {
    id: root

    Image {
        id: logo

        anchors.centerIn: parent
        opacity: 0.3
        source: "../images/logo.png"
    }
}

